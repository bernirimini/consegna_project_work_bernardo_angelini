<?php
/**
 * Il file base di configurazione di WordPress.
 *
 * Questo file viene utilizzato, durante l’installazione, dallo script
 * di creazione di wp-config.php. Non è necessario utilizzarlo solo via
 * web, è anche possibile copiare questo file in «wp-config.php» e
 * riempire i valori corretti.
 *
 * Questo file definisce le seguenti configurazioni:
 *
 * * Impostazioni MySQL
 * * Prefisso Tabella
 * * Chiavi Segrete
 * * ABSPATH
 *
 * È possibile trovare ultetriori informazioni visitando la pagina del Codex:
 *
 * @link https://codex.wordpress.org/it:Modificare_wp-config.php
 *
 * È possibile ottenere le impostazioni per MySQL dal proprio fornitore di hosting.
 *
 * @package WordPress
 */

// ** Impostazioni MySQL - È possibile ottenere queste informazioni dal proprio fornitore di hosting ** //
/** Il nome del database di WordPress */
define('DB_NAME', 'project-work');

/** Nome utente del database MySQL */
define('DB_USER', 'ifts');

/** Password del database MySQL */
define('DB_PASSWORD', 'ifts1');

/** Hostname MySQL  */
define('DB_HOST', 'localhost');

/** Charset del Database da utilizzare nella creazione delle tabelle. */
define('DB_CHARSET', 'utf8');

/** Il tipo di Collazione del Database. Da non modificare se non si ha idea di cosa sia. */
define('DB_COLLATE', '');

/**#@+
 * Chiavi Univoche di Autenticazione e di Salatura.
 *
 * Modificarle con frasi univoche differenti!
 * È possibile generare tali chiavi utilizzando {@link https://api.wordpress.org/secret-key/1.1/salt/ servizio di chiavi-segrete di WordPress.org}
 * È possibile cambiare queste chiavi in qualsiasi momento, per invalidare tuttii cookie esistenti. Ciò forzerà tutti gli utenti ad effettuare nuovamente il login.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         't>lEu-Yd|wBT+BXUe|ervUQoB@B7>h#D++q>n]1U<+n(&,.(Hy--*lRPe)y2@)+O');
define('SECURE_AUTH_KEY',  '!zN$K H{+iRY84|LWV^r1Hi?)Qy3g|W%ZHhrhlGRh 4@Flk#W7mKWYu4VpOYB`+D');
define('LOGGED_IN_KEY',    ']b<7h>h,|,9XU!eoqShLY @N7d-KF,.;oO@]{w=Opkd{rVA,{L(kEhN]^(+zls#~');
define('NONCE_KEY',        'sSgjVl_($$<@5nV}p9B`-_h`%MRPf!21!,UwxxQx-$XJGspKK*8hx-LUifx.Ul8|');
define('AUTH_SALT',        'CeHhwp2%$)]JbabFBV$^2s!Lw8[K|U dtC~_y5I<R@fw|xl/-{y|T-*QQQ|m@;Li');
define('SECURE_AUTH_SALT', 'lFo+sevz,+8W ?TP}9dw*-q4Oi-Qkhc{/(iX-] k>T.X*^toL-WT,>q5-nQ6d(E[');
define('LOGGED_IN_SALT',   ':xNxs$=FiIm2tqcB^_B]1j#zV-&[w``IVK_Yq8X+;%tjI53]IAfpd$hxuUk|^:B}');
define('NONCE_SALT',       '9=Hd-8qc~bAHpNCy|3MQ1].V[&l1~cKF$f3QwTW2Og4=vr]G|ENU|6}3Bp3p)@fV');

/**#@-*/

/**
 * Prefisso Tabella del Database WordPress.
 *
 * È possibile avere installazioni multiple su di un unico database
 * fornendo a ciascuna installazione un prefisso univoco.
 * Solo numeri, lettere e sottolineatura!
 */
$table_prefix = 'wp_';

/**
 * Per gli sviluppatori: modalità di debug di WordPress.
 *
 * Modificare questa voce a TRUE per abilitare la visualizzazione degli avvisi
 * durante lo sviluppo.
 * È fortemente raccomandato agli svilupaptori di temi e plugin di utilizare
 * WP_DEBUG all’interno dei loro ambienti di sviluppo.
 */
define('WP_DEBUG', false);

/* Finito, interrompere le modifiche! Buon creazione di contenuti. */

/** Path assoluto alla directory di WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Imposta le variabili di WordPress ed include i file. */
require_once(ABSPATH . 'wp-settings.php');