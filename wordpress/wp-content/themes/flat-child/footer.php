<?php
/**
 * The template for displaying the footer
 */
?>
			<?php if ( apply_filters( 'show_flat_credits', true ) ) : ?>
				<?php flat_hook_footer_before(); ?>
				<footer class="site-info" itemscope itemtype="http://schema.org/WPFooter">

				<?php //creo il mio footer bernardo angelini 
				echo do_shortcode('[do_widget id=text-3]'); ?>

				</footer>
				<?php flat_hook_footer_after(); ?>
			<?php endif; ?>
				<?php// flat_hook_content_bottom(); ?>
			</div>
			<?php// flat_hook_content_after(); ?>
		</div>
	</div>
</div>
<?php flat_hook_body_bottom(); ?>
<?php wp_footer(); ?>
</body>
</html>
