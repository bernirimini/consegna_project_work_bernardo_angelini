<?php get_header(); ?>
			<?php flat_hook_page_before(); ?>
			<div itemscope itemtype="http://schema.org/Article" id="content" class="site-content" role="main">
				<?php flat_hook_page_top(); ?>

			<?php while ( have_posts() ) : the_post(); ?>
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<header class="entry-header">
						<h1 class="entry-title" itemprop="name"><?php the_title(); ?></h1>

	<?php					
		$s="";
        $s1="";
       
        $titolo=get_permalink();
        $titolo=rtrim($titolo, '/');
        $l=explode('/', $titolo);
        $s1=$l[count($l)-1];
        $s1=explode('-', $s1);
        if($s1[0]=="luoghi"){
			echo "<h4>La Storia:</h4>".
			(get_post_meta( get_the_ID(), 'la_storia', true ));
			echo "<h4>Accessibilità per disabili:</h4>".
			(get_post_meta( get_the_ID(), 'accessibilita_per_disabili', true ));
			echo "<h4>Prezzo di ingresso:</h4>".
			(get_post_meta( get_the_ID(), 'prezzo_di_ingresso', true ));
		}



?>





					</header>
					<?php flat_hook_entry_before(); ?>
					<div class="entry-content" itemprop="articleBody">
						<?php flat_hook_entry_top(); ?>
						<?php the_content( __( 'Continue reading <span class="meta-nav">...</span>', 'flat' ) ); ?>
						<?php wp_link_pages(
							array(
							'before' => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'flat' ) . '</span>',
							'after' => '</div>',
							'link_before' => '<span>',
							'link_after' => '</span>',
							)
						); ?>
						<?php flat_hook_entry_bottom(); ?>
					</div>
					<?php flat_hook_entry_after(); ?>
				</article>
				<?php// comments_template(); ?>
			<?php endwhile; ?>

				<?php flat_hook_page_bottom(); ?>
			</div>
			<?php flat_hook_page_after(); ?>
<?php get_footer(); ?>
