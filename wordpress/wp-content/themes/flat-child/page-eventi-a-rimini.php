
<?php get_header(); ?>
			<?php flat_hook_page_before(); ?>
			<div itemscope itemtype="http://schema.org/Article" id="content" class="site-content" role="main">
				<?php flat_hook_page_top(); ?>

			
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<header class="entry-header">
						<h1 class="entry-title" itemprop="name"><?php the_title(); ?></h1>
					</header>
					<?php flat_hook_entry_before(); ?>
					<div class="entry-content" itemprop="articleBody">




<!-- loop--------------------------------------------- -->
				<?php 
    
    $args=array(
        'post_type' => 'evento',
        'post_status' => 'publish',
        'posts_per_page' => -1,
        'orderby' => 'date',
        'order' => 'desc'
    );
    
    query_posts($args);
    if(have_posts()){
        while(have_posts()){
            the_post();
    
    ?>
            <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
               
                    <h3 >
                        <a href="<?php the_permalink(); ?>"><?php the_title();?></a>
                    </h3>
                    <!-- <span class="entry-date">Data pubblicazione: <?php// the_date(); ?> </span><br> -->

                    Data dell'evento:
		<?php echo	(get_post_meta( get_the_ID(), 'data_dell_evento', true ))  ?><br>
		Luogo dell'evento:
		<?php echo	(get_post_meta( get_the_ID(), 'luogo_evento', true ))  ?>

              
                
                <!-- <div class="entry-content"><//?php the_content();?></div> -->
                
				</article>
    
    <?php       
        }
    }
    
    wp_reset_query();
    
	?>
	
<!-- loop--------------------------------------------- -->


</div>
					<?php flat_hook_entry_after(); ?>
				
				
			

				<?php flat_hook_page_bottom(); ?>
			</div>
			<?php flat_hook_page_after(); ?>
<?php get_footer(); ?>
