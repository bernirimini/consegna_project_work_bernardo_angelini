<?php get_header(); ?>
			<?php flat_hook_page_before(); ?>
			<div itemscope itemtype="http://schema.org/Article" id="content" class="site-content" role="main">
				<?php flat_hook_page_top(); ?>

			<?php while ( have_posts() ) : the_post(); ?>
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<header class="entry-header">
						<h1 class="entry-title" itemprop="name"><?php the_title(); ?></h1>





                        <h4>Principali luoghi di interesse a Rimini:</h4><br>
                    <?php //get_template_part( 'content', 'page' ); 

                    $args = array(
                        'offset' => 0,
                        'category' => '',
                        'category_name' => '',
                        'orderby' => 'date',
                        'order' => 'DESC',
                        'include' => '', 'exclude' => '',
                        'meta_key' => '',
                        'meta_value' => '',
                        'post_type' => 'page', 'post_mime_type' => '',
                        'post_parent' => '',
                        'author' => '',
                        'post_status' => 'publish',
                        'suppress_filters' => true
                        );
                        $the_query = new WP_Query( $args );

                        // The Loop
                        if ( $the_query->have_posts() ) {
                        echo '<ul>';
                        while ( $the_query->have_posts() ) {
                        
                
                        //if( strpos($titolo, 'luoghi-') == true )
                        
                        $the_query->the_post();
                        $s="";
                        $s1="";
                      
                        $titolo=get_permalink();
                        $titolo=rtrim($titolo, '/');
                        $l=explode('/', $titolo);
                        $s1=$l[count($l)-1];
                        $s1=explode('-', $s1);
                        
                        
                        if($s1[0]=="luoghi")
                        {
                        for($i=0;$i<count($l);$i++)
                        {
                            $s.=$s1[$i];
                            //$s.=$s1[$i];
                            if($i<count($l))
                            {
                                $s.='-';
                            }
                        }
                    
                        $s=rtrim($s,'-');
                        
                        echo '<li><a href="//127.0.0.1/project_work_bernardo_angelini/wordpress/'.$s.'">' . get_the_title() . '</a></li>';
                    
                        }
                        }
                        echo '</ul>';
                        /* Restore original Post Data */
                        wp_reset_postdata();
                        }

                    $l=get_the_permalink();
                    $l=explode('/', $l);
                    $s=$l[4];
                    //articoli_per_pagine();

                    ?>
                    <!-- end content container -->




					</header>
					<?php flat_hook_entry_before(); ?>
					<div class="entry-content" itemprop="articleBody">
						<?php flat_hook_entry_top(); ?>
						<?php the_content( __( 'Continue reading <span class="meta-nav">...</span>', 'flat' ) ); ?>
						<?php wp_link_pages(
							array(
							'before' => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'flat' ) . '</span>',
							'after' => '</div>',
							'link_before' => '<span>',
							'link_after' => '</span>',
							)
						); ?>
						<?php flat_hook_entry_bottom(); ?>
					</div>
					<?php flat_hook_entry_after(); ?>
				</article>
				<?php //comments_template(); ?>
			<?php endwhile; ?>

				<?php flat_hook_page_bottom(); ?>
			</div>
			<?php flat_hook_page_after(); ?>
<?php get_footer(); ?>
